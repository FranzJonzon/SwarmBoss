using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Flock/Behaivor/Avoidance")]
public class AvoidanceBehaivor : FilteredFlockBehaivor
{
    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        //if no neighors, return no adjustment
        if (context.Count == 0)
            return Vector3.zero;

        //add all points togheter and avrage
        Vector3 avoidanceMove = Vector3.zero;
        int nAvoide = 0;
        List<Transform> filteredContext = (filter == null) ? context : filter.Filter(agent, context);
        foreach (Transform item in filteredContext)
        {
            if(Vector3.SqrMagnitude(item.position  -agent.transform.position)< flock.SquareAvoidanceRadious)
            {
                nAvoide++;
                avoidanceMove += agent.transform.position-item.position;
            }
  
        }
        if (nAvoide > 0)
            avoidanceMove /= nAvoide;

        return avoidanceMove;

    }
}
