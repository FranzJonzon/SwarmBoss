using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Flock/Behaivor/Alignment")]
public class AligmentBehaivor : FilteredFlockBehaivor
{
    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        //if no neighors, maintein current alligmnet
        if (context.Count == 0)
            return agent.transform.forward;

        //add all points togheter and avrage
        Vector3 alignmentMove = Vector3.zero;
        List<Transform> filteredContext = (filter == null) ? context : filter.Filter(agent, context);
        foreach (Transform item in filteredContext)
        {
            alignmentMove += item.forward;
        }
        alignmentMove /= context.Count;

        return alignmentMove;
    }
}
