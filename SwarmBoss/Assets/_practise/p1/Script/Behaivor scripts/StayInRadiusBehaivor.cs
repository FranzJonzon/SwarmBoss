using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Flock/Behaivor/StayInRadious")]
public class StayInRadiusBehaivor : FlockBehaivor
{
    public Vector3 center;
    public float radius = 15f;

    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        Vector3 centerOffcet = center - agent.transform.position;
        float t = centerOffcet.magnitude / radius;
        if(t < 0.9f)
        {
            return Vector3.zero;
        }
        return centerOffcet * t * t;
    }
}
