using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behaivor/Composite")]
public class CompositBehaivor : FlockBehaivor
{

    public FlockBehaivor[] behaivors;
    public float[] weights;


    public override Vector3 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        // handle data missmatch
        if (weights.Length != behaivors.Length)
        {
            Debug.LogError("Data mismatch in " + name, this);
            return Vector3.zero;

        }

        //set up move
        Vector3 move = Vector3.zero;

        //iterate throug behaivors
        for(int i = 0; i < behaivors.Length; ++i)
        {
            Vector3 partialMove = behaivors[i].CalculateMove(agent, context, flock) * weights[i];

            if(partialMove != Vector3.zero)
            {
                if(partialMove.sqrMagnitude > weights[i] * weights[i])
                {
                    partialMove.Normalize();
                    partialMove *= weights[i];
                }
                move += partialMove;
            }
        }
        return move;
       
    }
}
