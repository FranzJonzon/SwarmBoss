using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{

    public FlockAgent agentPrefeb;
    List<FlockAgent> agents = new List<FlockAgent>();
    
    public FlockBehaivor behaivor;

    [Range(10, 500)]
    public int startingCount = 250;

    const float agentDensity = 0.08f;

    [Range(1f, 100f)]
    public float driveFactor = 10f;
    [Range(1f, 100f)]
    public float maxSpeed = 5f;
    [Range(1f, 10f)]
    public float neighborRadius = 1.5f;
    [Range(0f, 1f)]
    public float avoidanceRadiousMultiolier = 0.5f;

    float sqareMaxSpeed;
    float squareNeighborRadius;
    float squareAoidanceRadious;
    public float SquareAvoidanceRadious { get { return squareAoidanceRadious; } }

    // Start is called before the first frame update
    void Start()
    {
        sqareMaxSpeed         = maxSpeed * maxSpeed;
        squareNeighborRadius  = neighborRadius * neighborRadius;
        squareAoidanceRadious = squareNeighborRadius * avoidanceRadiousMultiolier * avoidanceRadiousMultiolier;
    
        for(int i = 0; i <startingCount; ++i)
        {
            FlockAgent newAgent = Instantiate(
                agentPrefeb, 
                Random.insideUnitSphere * startingCount * agentDensity,
                Quaternion.Euler(Random.Range(0f,360f), Random.Range(0f, 360f), Random.Range(0f, 360f)),
                transform
                );
            newAgent.name = "Agent " + i;
            newAgent.Initialize(this);
            agents.Add(newAgent);
        }
    
    
    }
    //ctrl k ctrl u => avkometera
    //ctrl k ctrl c => kometera

    void Update()
    {
        foreach(FlockAgent agent in agents)
        {
            List<Transform> context = GetNearbyObjects(agent);
            //for demo only to se it work
            //agent.GetComponentInChildren<MeshRenderer>().material.color = Color.Lerp(Color.white, Color.red, context.Count / 6f);

            Vector3 move = behaivor.CalculateMove(agent, context, this);
            move *= driveFactor;
            if (move.sqrMagnitude > sqareMaxSpeed)
            {
                move = move.normalized * maxSpeed;
            }
            agent.Move(move);
        }
    }

    List<Transform> GetNearbyObjects(FlockAgent agent)
    {
        List<Transform> context = new List<Transform>();
        Collider[] contectColliders = Physics.OverlapSphere(agent.transform.position, neighborRadius);
        foreach(Collider c in contectColliders)
        {
            if(c != agent.AgentCollider)
            {
                context.Add(c.transform);
            }
        }
        return context;
    }
}
