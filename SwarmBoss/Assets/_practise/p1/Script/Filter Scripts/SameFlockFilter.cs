using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Filter/Same Flock")]
public class SameFlockFilter : Contextfilter
{

    public override List<Transform> Filter(FlockAgent agent, List<Transform> originale)
    {
        List<Transform> filtered = new List<Transform>();

        foreach(Transform item in originale)
        {
            FlockAgent itemAgent = item.GetComponent<FlockAgent>();
            if(itemAgent != null && itemAgent.AgentFlock == agent.AgentFlock)
            {
                filtered.Add(item);
            }
        }

        return filtered;
    }
}
