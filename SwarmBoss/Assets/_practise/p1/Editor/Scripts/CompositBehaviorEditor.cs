using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CompositBehaivor))]
public class CompositBehaviorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        //return;

        // setupp
        CompositBehaivor cb = (CompositBehaivor)target;


        EditorGUILayout.BeginHorizontal();

        //check for behaivor
        if (cb.behaivors == null || cb.behaivors.Length == 0)
        {
            EditorGUILayout.HelpBox("No bheivors in array.", MessageType.Warning);

            EditorGUILayout.EndHorizontal();
        }
        else
        {

            EditorGUILayout.LabelField("Number", GUILayout.MinWidth(60f), GUILayout.MaxWidth(60f));

            EditorGUILayout.LabelField("Behaviors", GUILayout.MinWidth(60f));

            EditorGUILayout.LabelField("Weights", GUILayout.MinWidth(60f), GUILayout.MaxWidth(60f));

            EditorGUILayout.EndHorizontal();

            EditorGUI.BeginChangeCheck();
            for (int i = 0; i < cb.behaivors.Length; ++i)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField(i.ToString(), GUILayout.MinWidth(60f), GUILayout.MaxWidth(60f));
                cb.behaivors[i] = (FlockBehaivor)EditorGUILayout.ObjectField(cb.behaivors[i], typeof(FlockBehaivor), false, GUILayout.MinWidth(60f));
                cb.weights[i] = EditorGUILayout.FloatField(cb.weights[i], GUILayout.MinWidth(60f), GUILayout.MaxWidth(60f));
                EditorGUILayout.EndHorizontal();
            }
            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(cb);
            }
        }

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Add behaivor"))
        {
            AddBehaivor(cb);
            EditorUtility.SetDirty(cb);
        }

        if (cb.behaivors != null && cb.behaivors.Length > 0)
        {
            if (GUILayout.Button("Remove behaivor"))
            {
                RemoveBehaivor(cb);
                EditorUtility.SetDirty(cb);
            }
        }
        EditorGUILayout.EndHorizontal();






    }


    void AddBehaivor(CompositBehaivor cb)
    {
        int oldCount = (cb.behaivors != null) ? cb.behaivors.Length : 0;
        FlockBehaivor[] newBehaivors = new FlockBehaivor[oldCount + 1];
        float[] newWeights = new float[oldCount + 1];
        for (int i = 0; i < oldCount; ++i)
        {
            newBehaivors[i] = cb.behaivors[i];
            newWeights[i] = cb.weights[i];
        }
        newWeights[oldCount] = 1f;
        cb.behaivors = newBehaivors;
        cb.weights = newWeights;
    }

    void RemoveBehaivor(CompositBehaivor cb)
    {
        int oldCount = cb.behaivors.Length;
        if (oldCount == 1)
        {
            cb.behaivors = null;
            cb.weights = null;
        }
        FlockBehaivor[] newBehaivors = new FlockBehaivor[oldCount - 1];
        float[] newWeights = new float[oldCount - 1];
        for (int i = 0; i < oldCount - 1; ++i)
        {
            newBehaivors[i] = cb.behaivors[i];
            newWeights[i] = cb.weights[i];
        }

        cb.behaivors = newBehaivors;
        cb.weights = newWeights;
    }

}
